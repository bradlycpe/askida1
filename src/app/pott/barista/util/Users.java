package app.pott.barista.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;

import org.apache.tomcat.jni.Time;

import app.pott.barista.Constants;

public class Users {
	public static String generate(String realIp) throws UnsupportedEncodingException, NoSuchAlgorithmException {
		// Create hash arraylist
		ArrayList<byte[]> toHash = new ArrayList<byte[]>();
		toHash.add(realIp.getBytes(Constants.ASIATICO_STRING_DEFAULT_ENCODING));
		toHash.add(new Long(Time.now()).toString().getBytes(Constants.ASIATICO_STRING_DEFAULT_ENCODING));

		// Hash the arraylist
		MessageDigest sha512 = MessageDigest.getInstance(Constants.ASIATICO_REGISTER_USER_HASH_ALGORITHM);
		for (byte[] b : toHash)
			sha512.update(b);
		return Base64.getEncoder().encodeToString(sha512.digest());
	}
}
