package app.pott.barista.gitea;

import java.io.IOException;
import java.net.URL;

import org.junit.jupiter.api.Test;

class GiteaTest {

	@Test
	void test() {
		try {
			Gitea gitea = new Gitea(new URL("https://git.pott.app/api/v1"), "57c5aa0b69bd1ba9efcd903148c781ecb8ceb372");
			gitea.openIssue("kaffeepott", "Android-Client", new Issue("Test", "HelloWorld", 2));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
