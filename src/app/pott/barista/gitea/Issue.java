package app.pott.barista.gitea;

import org.json.JSONArray;
import org.json.JSONObject;

import app.pott.barista.Constants.GiteaC;

/**
 * Basic representation of a gitea issue.
 * 
 * @author epileptic
 */
public class Issue {
	private String title;
	private String body;
	private int[] labels;

	public Issue(String title, String message, int... labels) {
		this.title = title;
		this.body = message;
		this.labels = labels;
	}

	public int[] getLabels() {
		return labels;
	}

	public String getBody() {
		return body;
	}

	public String getTitle() {
		return title;
	}

	/**
	 * Return the JSON api content for gitea.
	 */
	@Override
	public String toString() {
		JSONObject object = new JSONObject();

		object.put(GiteaC.JSON_TITLE, title);
		object.put(GiteaC.JSON_BODY, body);
		object.put(GiteaC.JSON_LABELS, new JSONArray(labels));

		return object.toString();
	}
}
