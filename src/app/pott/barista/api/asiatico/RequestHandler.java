package app.pott.barista.api.asiatico;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.LinkedList;

import org.apache.tomcat.jni.Time;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import app.pott.barista.BaristaResources;
import app.pott.barista.Constants;
import app.pott.barista.Constants.JsonC;
import app.pott.barista.Constants.QueriesC.AsiaticoQ;
import app.pott.barista.Constants.TextC;
import app.pott.barista.database.DatabaseAdapter;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.Badge;
import app.pott.barista.database.model.entities.Cafe;
import app.pott.barista.database.model.entities.CafeProduct;
import app.pott.barista.database.model.entities.CafeRating;
import app.pott.barista.database.model.entities.LocalizedProductType;
import app.pott.barista.database.model.entities.OpeningRange;
import app.pott.barista.database.model.entities.RatingMeta;
import app.pott.barista.database.model.entities.User;
import app.pott.barista.database.model.ticket.CompanionType;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.log.Log;
import app.pott.barista.util.IDs;
import app.pott.barista.util.KaffeePottLocale;

public class RequestHandler {

	private AbstractDataManager adm;

	public RequestHandler(AbstractDataManager adm) {
		this.adm = adm;
	}

	public JSONArray getcafes(JSONObject args) throws JSONException, SQLException, BaristaException {
		// Load cafes
		LinkedList<Cafe> rawCafes = null;
		try {
			rawCafes = adm.getCafesInRange(args.getDouble(JsonC.LONGITUDE), args.getDouble(JsonC.LATITUDE),
					args.getInt(JsonC.DISTANCE));

			JSONArray cafes = new JSONArray();
			for (final Cafe c : rawCafes) {
				//// CAFE JSON
				// Build root
				JSONObject cafeJson = new JSONObject();
				cafeJson.put(JsonC.NAME, c.getName());
				cafeJson.put(JsonC.CAFE_ID, c.getId());
				cafeJson.put(JsonC.LONGITUDE, c.getLongitude());
				cafeJson.put(JsonC.LATITUDE, c.getLatitude());

				//// OPENING RANGES
				// Prepare
				JSONArray openingRangeJson = new JSONArray();
				LinkedList<OpeningRange> rawOpeningRanges = adm.getOpeningRanges(c);
				rawOpeningRanges.forEach(r -> {
					JSONObject currentRange = new JSONObject();

					// Build
					currentRange.put(JsonC.DAY_OF_WEEK, r.getDay());

					currentRange.put(JsonC.OPENING_HOUR, r.getOpeningTime().toLocalDateTime().getHour());
					currentRange.put(JsonC.OPENING_MINUTE, r.getOpeningTime().toLocalDateTime().getMinute());

					currentRange.put(JsonC.CLOSING_HOUR, r.getClosingTime().toLocalDateTime().getHour());
					currentRange.put(JsonC.CLOSING_MINUTE, r.getClosingTime().toLocalDateTime().getMinute());

					// Add
					openingRangeJson.put(currentRange);
				});
				cafeJson.put(JsonC.OPENING_RANGES, openingRangeJson);

				//// RATINGS
				RatingMeta ratings = new RatingMeta(adm.getRatings(c));
				cafeJson.put(JsonC.RATING, ratings.getAvg());
				cafeJson.put(JsonC.RATING_COUNT, ratings.getRatingCount());

				//// PRODUCTS
				// Prepare
				JSONArray productsJson = new JSONArray();
				LinkedList<CafeProduct> rawProducts = adm.getCafeProducts(c);
				rawProducts.forEach(p -> {
					JSONObject currentProduct = new JSONObject();

					// Build
					currentProduct.put(JsonC.CAFE_ID, c.getId());
					currentProduct.put(JsonC.PRODUCT_TYPE_NAME, p.getProductTypeName());
					currentProduct.put(JsonC.SIZE, p.getSize());
					currentProduct.put(JsonC.PRICE, p.getPrice());

					// Add
					productsJson.put(currentProduct);
				});
				cafeJson.put(JsonC.PRODUCTS, productsJson);

				//// BADGES
				// Prepare
				JSONArray badgesJson = new JSONArray();
				LinkedList<Badge> rawBadges = adm.getBadges(c);
				rawBadges.forEach(b -> {
					badgesJson.put(b.getBadgeType());
				});
				cafeJson.put(JsonC.BADGES, badgesJson);

				//// INSERT INTO CAFE LIST
				cafes.put(cafeJson);
			}
			return cafes;
		} catch (JSONException | SQLException e) {
			throw Exf.newWrappedArgument(e);
		} catch (BaristaException e) {
			throw e;
		}
	}

	public JSONObject getProdTypeHash(JSONObject args, String defaultLocale) throws BaristaException {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance(Constants.ASIATICO_GET_PROD_TYPE_HASH_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			throw new BaristaException(e, ErrorType.PANIC);
		}

		// Try to create valid locale
		KaffeePottLocale locale;
		try {
			locale = new KaffeePottLocale(args.getString(JsonC.LOCALE));
		} catch (BaristaException e) {
			try {
				locale = new KaffeePottLocale(defaultLocale);
			} catch (BaristaException e1) {
				throw new BaristaException(e1, ErrorType.PANIC);
			}
		}

		try {
			// Load product types from db
			LocalizedProductType[] types = adm.getLocalizedProductTypes(locale).toArray(new LocalizedProductType[0]);
			try {

				// Iterate product types
				for (LocalizedProductType c : types) {

					// Hash type name and translation
					digest.update(c.getTypeName().getBytes(Constants.ASIATICO_STRING_DEFAULT_ENCODING));
					digest.update(c.getTypeNameLocalized().getBytes(Constants.ASIATICO_STRING_DEFAULT_ENCODING));

				}
			} catch (UnsupportedEncodingException e) {
				throw new BaristaException(e, ErrorType.PANIC);
			}
		} catch (SQLException e) {
			throw Exf.newWrappedArgument(e);
		}

		// Store in JSON as base64 string
		JSONObject response = new JSONObject();
		response.put(JsonC.HASH, Base64.getEncoder().encodeToString(digest.digest()));

		return response;
	}

	public JSONObject openTicket(JSONObject args) throws JSONException, BaristaException, SQLException {
		CompanionType ticketOperation = CompanionType.valueOf(args.getString(JsonC.OPERATION));

		try (Connection cnn = BaristaResources.getInstance().getKafffeepottDb().getConnection()) {

			DatabaseAdapter db = new DatabaseAdapter(cnn);

			JSONObject returnVal = new JSONObject();
			returnVal.put(JsonC.OK, true);

			// Check whether the userid is given or not
			if (!args.has(JsonC.USER_ID))
				throw Exf.newMissingUrlParameter(JsonC.USER_ID);

			// Check if all parameters needed for this operation are given
			if (!ticketOperation.isValid(args))
				throw Exf.newInvalidUrlParameterValue(ticketOperation.toString(), args.toString());

			// Extract globally used values
			String userId = args.getString(JsonC.USER_ID);
			JSONObject select = args.getJSONObject(JsonC.SELECT);
			JSONObject change = args.getJSONObject(JsonC.CHANGE);

			// Execute operation
			switch (ticketOperation) {
			case ADD_CAFE:
				long pseudoId = IDs.getRandomValidLong(db);
				db.cachedQuery(AsiaticoQ.TICKET_ADD_CAFE, userId, pseudoId, change.get(JsonC.NAME),
						change.getDouble(JsonC.LONGITUDE), change.getDouble(JsonC.LATITUDE),
						ticketOperation.toString());
				returnVal.put(JsonC.PSEUDO_ID, pseudoId);
				break;

			case REMOVE_CAFE:
				db.cachedQuery(AsiaticoQ.TICKET_REMOVE_CAFE, userId, select.get(JsonC.CAFE_ID),
						ticketOperation.toString());
				break;

			case CHANGE_CAFE_NAME:
				db.cachedQuery(AsiaticoQ.TICKET_CHANGE_CAFE_NAME, userId, select.getLong(JsonC.CAFE_ID),
						change.getString(JsonC.NAME), ticketOperation.toString());
				break;

			case CHANGE_CAFE_LOCATION:
				db.cachedQuery(AsiaticoQ.TICKET_CHANGE_CAFE_LOCATION, userId, select.getLong(JsonC.CAFE_ID),
						change.getDouble(JsonC.LATITUDE), change.getDouble(JsonC.LONGITUDE),
						ticketOperation.toString());
				break;

			case ADD_BADGE:
				db.cachedQuery(AsiaticoQ.TICKET_ADD_BADGE, userId, select.getLong(JsonC.CAFE_ID),
						change.getString(JsonC.BADGE).toLowerCase(), ticketOperation.toString());
				break;

			case REMOVE_BADGE:
				db.cachedQuery(AsiaticoQ.TICKET_REMOVE_BADGE, userId, select.getLong(JsonC.CAFE_ID),
						change.getString(JsonC.BADGE).toLowerCase(), ticketOperation.toString());
				break;

			case CHANGE_CAFE_OPENING_RANGES:

				// Create ticket
				PreparedStatement statement = db.getConnection().prepareStatement(
						AsiaticoQ.TICKET_CHANGE_CAFE_OPENING_RANGES, PreparedStatement.RETURN_GENERATED_KEYS);
				Object[] queryParameter = { userId, select.getLong(JsonC.CAFE_ID), ticketOperation.toString() };
				for (int i = 0; i < queryParameter.length; i++) {
					statement.setObject(i + 1, queryParameter[i]);
				}
				statement.executeUpdate();

				// Retrieve generated id
				ResultSet idResult = statement.getGeneratedKeys();
				idResult.next();
				long openingTicketId = idResult.getLong(1);

				JSONArray ranges = change.getJSONArray(JsonC.OPENING_RANGES);

				// Check integrity of opening ranges
				boolean valid = true;
				for (int i = 0; i < ranges.length() && valid; i++) {
					JSONObject c = ranges.getJSONObject(i);
					c.getString(JsonC.DAY_OF_WEEK);
					c.getInt(JsonC.OPENING_HOUR);
					c.getInt(JsonC.OPENING_MINUTE);
					c.getInt(JsonC.CLOSING_HOUR);
					c.getInt(JsonC.CLOSING_MINUTE);
					if (c.length() > 5) {
						valid = false;
						break;
					}
				}

				// If integrity ok, add to db
				if (valid) {

					// Make sure that opening ranges do not exceed maximum
					if (ranges.length() < Constants.OPENING_RANGE_TICKET_LIMIT) {
						DateFormat format = new SimpleDateFormat(Constants.OPENING_RANGE_DATE_FORMAT);

						// Iterate over ranges
						for (int i = 0; i < ranges.length() && valid; i++) {
							JSONObject currentRange = ranges.getJSONObject(i);

							// Try to add to db
							try {
								db.cachedQuery(AsiaticoQ.RANGE_CHANGE_CAFE_OPENING_RANGES, openingTicketId,
										currentRange.getString(JsonC.DAY_OF_WEEK),
										new Timestamp(format.parse(currentRange.getInt(JsonC.OPENING_HOUR) + ":"
												+ currentRange.getInt(JsonC.OPENING_MINUTE)).getTime()),
										new Timestamp(format.parse(currentRange.getInt(JsonC.CLOSING_HOUR) + ":"
												+ currentRange.getInt(JsonC.CLOSING_MINUTE)).getTime()));
							} catch (SQLException | ParseException e) {
								e.printStackTrace();
							}
						}
					}
				}
				returnVal.put(JsonC.OK, false);
				break;

			case CHANGE_PRODUCT_PRICE:
				db.cachedQuery(AsiaticoQ.TICKET_CHANGE_PRODUCT_PRICE, userId, select.getLong(JsonC.CAFE_ID),
						select.getString(JsonC.NAME), select.getString(JsonC.SIZE), change.getDouble(JsonC.PRICE),
						ticketOperation.toString());
				break;

			case DELETE_PRODUCT:
				db.cachedQuery(AsiaticoQ.TICKET_DELETE_PRODUCT, userId, select.getLong(JsonC.CAFE_ID),
						select.getString(JsonC.NAME), select.getString(JsonC.SIZE), ticketOperation.toString());
				break;

			case ADD_PRODUCT:
				db.cachedQuery(AsiaticoQ.TICKET_ADD_PRODUCT, userId, select.getLong(JsonC.CAFE_ID),
						select.getString(JsonC.NAME), select.getString(JsonC.SIZE), change.getDouble(JsonC.PRICE),
						ticketOperation.toString());
				break;

			default:
				throw new BaristaException(TextC.INVALID_TICKET_OPERATION_ERR, ErrorType.ARGUMENT);
			}
			return returnVal;
		}
	}

	public JSONObject setRating(JSONObject args) throws JSONException, SQLException {
		adm.setRating(
				new CafeRating(args.getLong(JsonC.CAFE_ID), args.getString(JsonC.USER_ID), args.getInt(JsonC.RATING)));

		JSONObject result = new JSONObject();
		result.put(JsonC.OK, true);
		return result;
	}

	public JSONObject getProducts(JSONObject args, String defaultLocale) throws BaristaException, SQLException {

		// Determine locale
		KaffeePottLocale locale;
		try {
			locale = new KaffeePottLocale(args.getString(JsonC.LOCALE));
		} catch (BaristaException e) {
			try {
				locale = new KaffeePottLocale(defaultLocale);
			} catch (BaristaException e1) {
				throw new BaristaException(e1, ErrorType.PANIC);
			}
		}

		JSONObject responseJson = new JSONObject();

		// Get products
		JSONArray productsJson = new JSONArray();
		LinkedList<LocalizedProductType> products = adm.getLocalizedProductTypes(locale);
		products.forEach(p -> {
			JSONObject current = new JSONObject();
			current.put(JsonC.NAME, p.getTypeName());
			current.put(JsonC.TYPE_LOCALIZED, p.getTypeNameLocalized());
			productsJson.put(current);
		});
		responseJson.put(JsonC.DATA, productsJson);

		// Set localization
		responseJson.put(JsonC.LOCALE, locale.toString());

		return responseJson;
	}

	public JSONObject registerUser(String header)
			throws UnsupportedEncodingException, NoSuchAlgorithmException, SQLException {
		Log.d("User id request: " + header, this);

		// Create hash arraylist
		ArrayList<byte[]> toHash = new ArrayList<byte[]>();
		toHash.add(header.getBytes(Constants.ASIATICO_STRING_DEFAULT_ENCODING));
		toHash.add(new Long(Time.now()).toString().getBytes(Constants.ASIATICO_STRING_DEFAULT_ENCODING));

		// Hash the arraylist
		MessageDigest sha512 = MessageDigest.getInstance(Constants.ASIATICO_REGISTER_USER_HASH_ALGORITHM);
		for (byte[] b : toHash)
			sha512.update(b);
		String id = Base64.getEncoder().encodeToString(sha512.digest());

		adm.addUser(new User(id, "::1"));

		JSONObject result = new JSONObject();
		result.put(JsonC.OK, id);
		return result;
	}
}
