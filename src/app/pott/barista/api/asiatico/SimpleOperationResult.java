package app.pott.barista.api.asiatico;

public enum SimpleOperationResult {
	OK, ARGUMENT_ERROR, INTERNAL_ERROR
}
