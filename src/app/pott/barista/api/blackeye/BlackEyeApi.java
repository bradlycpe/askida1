package app.pott.barista.api.blackeye;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import app.pott.barista.BaristaResources;
import app.pott.barista.Constants.BlackEyeProtocolC;
import app.pott.barista.Constants.HttpC;
import app.pott.barista.Constants.MimeC;
import app.pott.barista.api.BaristaApi;
import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.DatabaseAdapter;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.http.RequestType;
import app.pott.barista.log.Log;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonObject;

/**
 * API for version BLACK_EYE
 * 
 * @author epileptic
 */
public class BlackEyeApi implements BaristaApi {

	@Override
	public void call(HttpServletRequest request, HttpServletResponse response, RequestType type)
			throws BaristaException {
		try {
			// Read body
			XJsonObject bodyJson = null;
			try (BufferedReader bodyReader = request.getReader()) {

				if (type != RequestType.GET) {
					// Read body
					StringBuilder sb = new StringBuilder();
					while (bodyReader.ready())
						sb.append(bodyReader.readLine());

					// Wrap body to JSON
					bodyJson = new XJsonObject(sb.toString());
				}
			} catch (IOException e) {
				throw Exf.newClientConnectionException(e);
			}

			// Check that function parameter is present
			String functionString = request.getParameter(BlackEyeProtocolC.FUNCTION_PARAMETER).toUpperCase();
			if (functionString == null)
				throw Exf.newMissingUrlParameter(BlackEyeProtocolC.FUNCTION_PARAMETER);

			try (Connection dbc = BaristaResources.getInstance().getKafffeepottDb().getConnection()) {
				AbstractDataManager adm = new AbstractDataManager(new DatabaseAdapter(dbc));

				// Execute API call
				BlackEyeFunction function = BlackEyeFunction.valueOf(functionString);
				request.getMethod();
				XJson result = function.call(HttpMethod.valueOf(request.getMethod()), request, bodyJson, adm);

				//// Respond
				// Set okay
				response.setStatus(HttpServletResponse.SC_OK);

				// Send response
				if (result != null) {
					String responsePlain = result.toJsonString();
					response.setHeader(HttpC.CONTENT_TYPE_HEADER, MimeC.APPLICATION_JSON);
					response.getWriter().append(responsePlain);
				}
			} catch (SQLException e) {
				throw Exf.newWrappedArgument(e);
			} catch (IllegalArgumentException e) {
				throw Exf.newInvalidUrlParameterValue(BlackEyeProtocolC.FUNCTION_PARAMETER, functionString);
			} catch (IOException e) {
				throw Exf.newClientConnectionException(e);
			}
		} catch (BaristaException e) {
			Log.d("BaristaException occured: " + e.getType() + " -> " + e.getMessage(), this);
			Log.x(e, this);

			// Set code
			switch (e.getType()) {
			case ARGUMENT:
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				break;

			default:
			case INTERNAL:
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				break;

			case SECURITY:
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				break;

			case PANIC:
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				break;
			}

			// Send content
			response.setHeader(HttpC.CONTENT_TYPE_HEADER, MimeC.APPLICATION_JSON);
			try {
				// Build error message JSON
				XJsonObject result = new XJsonObject();
				result.put(JsonC.ERROR_MESSAGE, e.toString());

				// Send
				response.getWriter().append(result.toJsonString());
			} catch (IOException e1) {
				Log.x(e1, this);
			}
		}
	}
}
