package app.pott.barista.api.blackeye.function;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.BaristaResources;
import app.pott.barista.aer.Aer;
import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonException;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to control the {@link Aer}.
 * 
 * @author epileptic
 */
public class ReportFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.POST;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {
		XJsonObject dataResultJson = new XJsonObject();

		try {
			// Get report information
			String stacktrace = body.getString(JsonC.STACKTRACE);
			String thread = body.getString(JsonC.THREAD);
			String apiLevel = String.valueOf(body.getLong(JsonC.API_LEVEL));
			String appVersion = String.valueOf(body.getLong(JsonC.APP_VERSION));

			// Report
			dataResultJson.put(JsonC.AER_RESULT, BaristaResources.getInstance().getAer()
					.reportIssue(stacktrace, thread, apiLevel, appVersion).toString());

			// Response
			XJsonObject rootResultJson = new XJsonObject();
			rootResultJson.put(JsonC.CONTENT, dataResultJson);
			return rootResultJson;
		} catch (XJsonException e) {
			throw Exf.newWrappedArgument(e);
		}
	}

}
