package app.pott.barista.api.blackeye.function;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.api.blackeye.BlackEyeProtocol.BEPDeserialize;
import app.pott.barista.api.blackeye.function.openticket.OpenTicketResult;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.OpeningRange;
import app.pott.barista.database.model.helper.TicketBuilder;
import app.pott.barista.database.model.product.ProductSize;
import app.pott.barista.database.model.ticket.CompanionType;
import app.pott.barista.database.model.ticket.TicketStatus;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonArray;
import app.pott.barista.util.json.XJsonException;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to open a ticket.
 * 
 * @author epileptic
 */
public class OpenTicketFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.POST;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {
		XJsonObject contentJson = new XJsonObject();

		try {
			TicketBuilder ticketBuilder = new TicketBuilder(body.getString(JsonC.USER_ID));

			// Separate select and change components
			XJsonObject select = body.getJsonObject(JsonC.SELECT);
			XJsonObject change = body.getJsonObject(JsonC.CHANGE);

			// Insert values
			if (select.has(JsonC.CAFE_ID))
				ticketBuilder.setAffectedCafeId(select.getLong(JsonC.CAFE_ID));

			if (select.has(JsonC.PRODUCT_TYPE_NAME))
				ticketBuilder.setAffectedProductName(select.getString(JsonC.PRODUCT_TYPE_NAME));

			if (select.has(JsonC.PRODUCT_SIZE)) {
				String productSizeString = select.getString(JsonC.PRODUCT_SIZE);
				try {
					ProductSize productSize = ProductSize.valueOf(productSizeString);
					ticketBuilder.setAffectedProductSize(productSize);
				} catch (IllegalArgumentException e) {
					throw Exf.newWrappedArgument(e);
				}
			}

			if (body.has(JsonC.TICKET_OPERATION)) {
				String operationString = body.getString(JsonC.TICKET_OPERATION);
				try {
					CompanionType operation = CompanionType.valueOf(operationString);
					ticketBuilder.setCompanion(operation);
				} catch (IllegalArgumentException e) {
					throw Exf.newWrappedArgument(e);
				}
			}

			if (select.has(JsonC.BADGE_TYPE_NAME))
				ticketBuilder.setNewBadgeType(select.getString(JsonC.BADGE_TYPE_NAME));

			if (change.has(JsonC.CAFE_NAME))
				ticketBuilder.setNewCafeName(change.getString(JsonC.CAFE_NAME));

			if (change.has(JsonC.LONGITUDE))
				ticketBuilder.setNewLocationLon(change.getDouble(JsonC.LONGITUDE));

			if (change.has(JsonC.LATITUDE))
				ticketBuilder.setNewLocationLat(change.getDouble(JsonC.LATITUDE));

			if (change.has(JsonC.OPENING_RANGES)) {
				XJsonArray rangesJson = change.getArray(JsonC.OPENING_RANGES);
				LinkedList<OpeningRange> ranges = new LinkedList<OpeningRange>();
				for (int i = 0; i < rangesJson.size(); i++) {
					try {
						ranges.add(BEPDeserialize.buildOpeningRange(rangesJson.getJsonObject(i)));
					} catch (BaristaException e) {
						throw e;
					} catch (ParseException e) {
						throw new BaristaException(e, ErrorType.INTERNAL);
					}
				}
				ticketBuilder.setNewOpeningRanges(ranges);
			}

			if (change.has(JsonC.PRODUCT_PRICE))
				ticketBuilder.setNewProductPrice(change.getDouble(JsonC.PRODUCT_PRICE));

			ticketBuilder.setStatus(TicketStatus.UNRESOLVED);

			// Open the ticket
			OpenTicketResult otresult = adm.openTicket(ticketBuilder.build());
			long ticketId = otresult.getTicketId();
			contentJson.put(JsonC.TICKET_ID, ticketId);

			Long virtualCafeId = otresult.getValue();
			if (virtualCafeId != null)
				contentJson.put(JsonC.VIRTUAL_CAFE_ID, virtualCafeId.doubleValue());

		} catch (XJsonException e) {
			throw Exf.newWrappedArgument(e);
		} catch (SQLException e) {
			throw Exf.newWrappedArgument(e);
		} catch (NullPointerException e) {
			throw Exf.newWrappedArgument(e);
		}

		XJsonObject resultJson = new XJsonObject();
		resultJson.put(JsonC.CONTENT, contentJson);
		return resultJson;
	}

}
