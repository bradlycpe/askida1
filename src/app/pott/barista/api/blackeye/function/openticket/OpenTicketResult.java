package app.pott.barista.api.blackeye.function.openticket;

import app.pott.barista.database.model.AbstractDataManager;

/**
 * Dirty workaround for {@link AbstractDataManager} open ticket method.
 * 
 * ==> Dataclass
 * 
 * @author epileptic
 */
public class OpenTicketResult {
	private long ticketId;
	private Long value;

	public OpenTicketResult(long ticketId, Long value) {
		this.ticketId = ticketId;
		this.value = value;
	}

	public Long getValue() {
		return value;
	}

	public long getTicketId() {
		return ticketId;
	}
}
