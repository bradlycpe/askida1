package app.pott.barista.api.blackeye.function;

import java.sql.SQLException;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.api.blackeye.BlackEyeProtocol.BEPSerialize;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.Badge;
import app.pott.barista.database.model.entities.Cafe;
import app.pott.barista.database.model.entities.CafeProduct;
import app.pott.barista.database.model.entities.OpeningRange;
import app.pott.barista.database.model.entities.RatingMeta;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonArray;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to query cafes in a specific square surrounding the passed
 * coordinates.
 * 
 * @author epileptic
 */
public class ListCafesFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.GET;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {

		// Parameter checking
		double longitude;
		double latitude;
		int distance;
		try {
			longitude = Double.parseDouble(httpRequest.getParameter(JsonC.LONGITUDE));
		} catch (NumberFormatException | NullPointerException e) {
			throw Exf.newInvalidUrlParameterValue(JsonC.LONGITUDE, httpRequest.getParameter(JsonC.LONGITUDE));
		}
		try {
			latitude = Double.parseDouble(httpRequest.getParameter(JsonC.LATITUDE));
		} catch (NumberFormatException | NullPointerException e) {
			throw Exf.newInvalidUrlParameterValue(JsonC.LATITUDE, httpRequest.getParameter(JsonC.LATITUDE));
		}
		try {
			distance = Integer.parseInt(httpRequest.getParameter(JsonC.DISTANCE));
		} catch (NumberFormatException | NullPointerException e) {
			throw Exf.newInvalidUrlParameterValue(JsonC.DISTANCE, httpRequest.getParameter(JsonC.DISTANCE));
		}

		try {
			// Load cafes
			LinkedList<Cafe> cafes = adm.getCafesInRange(longitude, latitude, distance);

			// Generate json cafe objects
			XJsonArray contentJson = new XJsonArray();
			for (Cafe c : cafes) {
				LinkedList<CafeProduct> products = adm.getCafeProducts(c);
				LinkedList<OpeningRange> ranges = adm.getOpeningRanges(c);
				LinkedList<Badge> badges = adm.getBadges(c);
				contentJson.put(BEPSerialize.buildCafe(c, products, ranges, badges, new RatingMeta(adm.getRatings(c))));
			}

			// Build result
			XJsonObject result = new XJsonObject();
			result.put(JsonC.CONTENT, contentJson);
			return result;

		} catch (BaristaException e) {
			throw e;
		} catch (SQLException e) {
			throw new BaristaException(ErrorType.ARGUMENT);
		}
	}

}
