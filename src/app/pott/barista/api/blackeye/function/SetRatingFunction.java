package app.pott.barista.api.blackeye.function;

import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.CafeRating;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonException;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to set a rating of a user for a specific cafe.
 * 
 * @author epileptic
 */
public class SetRatingFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.PUT;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {
		try {
			adm.setRating(new CafeRating(body.getLong(JsonC.CAFE_ID), body.getString(JsonC.USER_ID),
					body.getInt(JsonC.RATING_VALUE)));
		} catch (SQLException e) {
			throw Exf.newWrappedArgument(e);
		} catch (XJsonException e) {
			throw Exf.newWrappedArgument(e);
		}

		return null;
	}

}
