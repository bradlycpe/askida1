package app.pott.barista.api.blackeye.function;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.SQLSyntaxErrorException;
import java.sql.SQLTransientException;
import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.api.blackeye.BlackEyeProtocol.BEPSerialize;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.LocalizedProductType;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.KaffeePottLocale;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonArray;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to query product types.
 * 
 * @author epileptic
 */
public class ListProductsFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.GET;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {

		// Ensure that locale is given
		String localeString = httpRequest.getParameter(JsonC.LOCALE);
		if (localeString == null)
			throw Exf.newMissingUrlParameter(JsonC.LOCALE);

		// Parse locale
		KaffeePottLocale localeObject;
		localeObject = new KaffeePottLocale(localeString);

		try {
			// Load types
			LinkedList<LocalizedProductType> localizedProducts = adm.getLocalizedProductTypes(localeObject);

			// Build JSON content
			XJsonArray localizedProductsJson = new XJsonArray();
			localizedProducts.forEach(c -> localizedProductsJson.put(BEPSerialize.buildLocalizedProductType(c)));

			// Build result JSON
			XJsonObject resultJson = new XJsonObject();
			resultJson.put(JsonC.LOCALE, localeObject.toString());
			resultJson.put(JsonC.CONTENT, localizedProductsJson);
			return resultJson;

		} catch (SQLSyntaxErrorException | SQLTransientException e) {
			throw Exf.newWrappedInternal(e);
		} catch (SQLIntegrityConstraintViolationException e) {
			throw Exf.newWrappedArgument(e);
		} catch (SQLException e) {
			throw Exf.newWrappedInternal(e);
		}
	}

}
