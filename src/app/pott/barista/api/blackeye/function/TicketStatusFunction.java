package app.pott.barista.api.blackeye.function;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.api.blackeye.BlackEyeConstants.JsonC;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.database.model.entities.Ticket;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonObject;

/**
 * API function used to check if a specific ticket has been accepted or
 * rejected.
 * 
 * @author epileptic
 */
public class TicketStatusFunction implements BlackEyeApiFunction {

	@Override
	public HttpMethod getSupportedMethod() {
		return HttpMethod.GET;
	}

	@Override
	public XJson call(HttpServletRequest httpRequest, XJsonObject body, AbstractDataManager adm)
			throws BaristaException {
		XJsonObject rootResultJson = new XJsonObject();
		XJsonObject dataResultJson = new XJsonObject();

		try {
			Ticket ticket = adm.getTicketById(Long.parseLong(httpRequest.getParameter(JsonC.TICKET_ID)));
			dataResultJson.put(JsonC.TICKET_STATUS, ticket.getStatus().toString());
		} catch (SQLIntegrityConstraintViolationException e) {
			throw Exf.newWrappedArgument(e);
		} catch (SQLException e) {
			throw Exf.newWrappedInternal(e);
		} catch (NumberFormatException e) {
			throw Exf.newWrappedArgument(e);
		} catch (NullPointerException e) {
			throw new BaristaException(e, ErrorType.PANIC);
		}

		rootResultJson.put(JsonC.CONTENT, dataResultJson);
		return rootResultJson;
	}

}
