package app.pott.barista.api.blackeye;

import javax.servlet.http.HttpServletRequest;

import app.pott.barista.api.blackeye.function.BlackEyeApiFunction;
import app.pott.barista.api.blackeye.function.CreateUserFunction;
import app.pott.barista.api.blackeye.function.ListCafesFunction;
import app.pott.barista.api.blackeye.function.ListProductsFunction;
import app.pott.barista.api.blackeye.function.OpenTicketFunction;
import app.pott.barista.api.blackeye.function.ProductHashFunction;
import app.pott.barista.api.blackeye.function.ReportFunction;
import app.pott.barista.api.blackeye.function.SetRatingFunction;
import app.pott.barista.api.blackeye.function.TicketStatusFunction;
import app.pott.barista.database.model.AbstractDataManager;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.util.HttpMethod;
import app.pott.barista.util.json.XJson;
import app.pott.barista.util.json.XJsonObject;

public enum BlackEyeFunction {
	LISTCAFES(new ListCafesFunction()), LISTPRODUCTS(new ListProductsFunction()),
	PRODUCTSHASH(new ProductHashFunction()), OPENTICKET(new OpenTicketFunction()),
	TICKETSTATUS(new TicketStatusFunction()), REPORT(new ReportFunction()), SETRATING(new SetRatingFunction()),
	CREATEUSER(new CreateUserFunction());

	private BlackEyeApiFunction function;

	private BlackEyeFunction(BlackEyeApiFunction function) {
		this.function = function;
	}

	public XJson call(HttpMethod method, HttpServletRequest request, XJsonObject body, AbstractDataManager manager)
			throws BaristaException {
		if (function.getSupportedMethod() != method)
			throw new BaristaException("Function '" + this.toString() + "' does not support the method '" + method
					+ "'! Use '" + function.getSupportedMethod() + "'", ErrorType.ARGUMENT);
		return function.call(request, body, manager);
	}
}
