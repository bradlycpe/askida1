package app.pott.barista.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A simplifying layer for database communication.
 * 
 * @author epileptic
 */
public class DatabaseAdapter {

	// Borrowed connection
	private Connection dbC;

	// Cached prepared statements
	private ConcurrentHashMap<String, PreparedStatement> cachedStatements;

	/**
	 * @param dbC {@link Connection} used for database connection.
	 */
	public DatabaseAdapter(Connection dbC) {
		this.dbC = dbC;
		this.cachedStatements = new ConcurrentHashMap<>();
	}

	/**
	 * Executes a database query and caches {@link PreparedStatement}s.
	 * 
	 * @param query        SQL query to be executed
	 * @param replacements {@link PreparedStatement} placeholder replacements
	 * @return {@link ResultSet}
	 * @throws SQLException
	 */
	public ResultSet cachedQuery(String query, Object... replacements) throws SQLException {

		// If query not cached, do that!
		if (!cachedStatements.containsKey(query))
			cachedStatements.put(query, dbC.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS));

		// Execute cached query
		PreparedStatement statement = cachedStatements.get(query);
		for (int i = 0; i < replacements.length; i++)
			statement.setObject(i + 1, replacements[i]);

		// Return query result
		return statement.executeQuery();
	}

	public long cachedGeneratedQuery(String query, Object... replacements) throws SQLException {
		// If query not cached, do that!
		if (!cachedStatements.containsKey(query))
			cachedStatements.put(query, dbC.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS));

		// Execute cached query
		PreparedStatement statement = cachedStatements.get(query);
		for (int i = 0; i < replacements.length; i++)
			statement.setObject(i + 1, replacements[i]);

		statement.executeUpdate();
		ResultSet keys = statement.getGeneratedKeys();
		keys.first();

		return keys.getLong(1);
	}

	/**
	 * Releases the database {@link Connection} back to the pool.
	 * 
	 * @throws SQLException
	 */
	public void free() throws SQLException {
		dbC.close();
	}

	/**
	 * @return {@link Connection} used to communicate with the database.
	 */
	public Connection getConnection() {
		return dbC;
	}
}
