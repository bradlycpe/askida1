package app.pott.barista.database.model;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import app.pott.barista.Constants;
import app.pott.barista.Constants.DbKeyC;
import app.pott.barista.Constants.PlaceholderC;
import app.pott.barista.Constants.QueriesC.AsiaticoQ;
import app.pott.barista.api.blackeye.function.openticket.OpenTicketResult;
import app.pott.barista.api.blackeye.function.openticket.TicketResultBuilder;
import app.pott.barista.Constants.TextC;
import app.pott.barista.database.DatabaseAdapter;
import app.pott.barista.database.model.entities.Badge;
import app.pott.barista.database.model.entities.Cafe;
import app.pott.barista.database.model.entities.CafeProduct;
import app.pott.barista.database.model.entities.CafeRating;
import app.pott.barista.database.model.entities.LocalizedProductType;
import app.pott.barista.database.model.entities.OpeningRange;
import app.pott.barista.database.model.entities.Size;
import app.pott.barista.database.model.entities.Ticket;
import app.pott.barista.database.model.entities.User;
import app.pott.barista.database.model.product.ProductSize;
import app.pott.barista.database.model.ticket.CompanionType;
import app.pott.barista.database.model.ticket.TicketStatus;
import app.pott.barista.exceptions.BaristaException;
import app.pott.barista.exceptions.ErrorType;
import app.pott.barista.exceptions.Exf;
import app.pott.barista.util.IDs;
import app.pott.barista.util.KaffeePottLocale;

public class AbstractDataManager {

	private DatabaseAdapter db;

	public AbstractDataManager(DatabaseAdapter dbAdapter) {
		this.db = dbAdapter;
	}

	/**
	 * Queries cafes in range around given position.
	 * 
	 * @param longitude position
	 * @param latitude  position
	 * @param distance  search radius around the position
	 * @throws BaristaException
	 * @throws SQLException
	 */
	public LinkedList<Cafe> getCafesInRange(double longitude, double latitude, long distance)
			throws BaristaException, SQLException {
		LinkedList<Cafe> cafes = new LinkedList<>();

		// Calculate location boundaries
		double earth = 6378.137;
		double pi = Math.PI;
		double m = (1 / ((2 * pi / 360) * earth)) / 1000;

		double lonLow = longitude - (distance * m) / Math.cos(latitude * (pi / 180));
		double lonHigh = longitude + (distance * m) / Math.cos(latitude * (pi / 180));

		double latLow = latitude - (distance * m);
		double latHigh = latitude + (distance * m);

		// Execute db query
		ResultSet queryResult;
		queryResult = db.cachedQuery(AsiaticoQ.LOAD_CAFES_LOCATION_RANGE, lonHigh, lonLow, latHigh, latLow);

		// Wrap result in objects
		while (queryResult.next()) {
			Cafe current = new Cafe(queryResult.getString(DbKeyC.NAME), queryResult.getLong(DbKeyC.ID),
					queryResult.getDouble(DbKeyC.LOCATION_LON), queryResult.getDouble(DbKeyC.LOCATION_LAT));
			cafes.add(current);
		}

		return cafes;
	}

	/**
	 * Queries opening ranges for specific cafe.
	 */
	public LinkedList<OpeningRange> getOpeningRanges(Cafe cafe) throws SQLException {
		LinkedList<OpeningRange> ranges = new LinkedList<>();

		ResultSet queryResult = db.cachedQuery(AsiaticoQ.LOAD_OPENING_RANGES_FOR_CAFE_ID, cafe.getId());
		while (queryResult.next())
			ranges.add(new OpeningRange(queryResult.getString(DbKeyC.DAY_OF_WEEK).toUpperCase(),
					queryResult.getTimestamp(DbKeyC.OPENING_TIME), queryResult.getTimestamp(DbKeyC.CLOSING_TIME)));

		return ranges;
	}

	/**
	 * Queries ratings for a specific cafe.
	 * 
	 * @param cafe
	 * @throws SQLException
	 */
	public LinkedList<CafeRating> getRatings(Cafe cafe) throws SQLException {
		LinkedList<CafeRating> ratings = new LinkedList<CafeRating>();

		ResultSet queryResult = db.cachedQuery(AsiaticoQ.LOAD_RATINGS_FOR_CAFE_ID, cafe.getId());
		while (queryResult.next())
			ratings.add(new CafeRating(cafe.getId(), queryResult.getString(DbKeyC.USER_ID),
					queryResult.getInt(DbKeyC.RATING)));

		return ratings;
	}

	/**
	 * Queries {@link CafeProduct} for a specific cafe.
	 */
	public LinkedList<CafeProduct> getCafeProducts(Cafe cafe) throws SQLException {
		LinkedList<CafeProduct> products = new LinkedList<CafeProduct>();

		ResultSet queryResult = db.cachedQuery(AsiaticoQ.LOAD_PRODUCTS_FOR_CAFE_ID, cafe.getId());
		while (queryResult.next())
			products.add(new CafeProduct(cafe.getId(), queryResult.getString(DbKeyC.PRODUCT_TYPE_NAME),
					Size.valueOf(queryResult.getString(DbKeyC.SIZE).toUpperCase()),
					queryResult.getDouble(DbKeyC.PRICE)));

		return products;
	}

	/**
	 * Queries {@link LocalizedProductType} for a specific cafe.
	 */
	public LinkedList<LocalizedProductType> getLocalizedProductTypes(KaffeePottLocale locale) throws SQLException {
		LinkedList<LocalizedProductType> types = new LinkedList<LocalizedProductType>();

		final String localeString = locale.toString();

		ResultSet queryResult = db.cachedQuery(
				AsiaticoQ.LOAD_LOCALIZED_PRODUCT_TYPES.replace(PlaceholderC.CONTENT, localeString.toString()));
		while (queryResult.next())
			types.add(new LocalizedProductType(queryResult.getString(DbKeyC.NAME),
					queryResult.getString(DbKeyC.NAME + "_" + locale.toString())));

		return types;
	}

	/**
	 * Queries {@link Badge} for a specific cafe
	 */
	public LinkedList<Badge> getBadges(Cafe cafe) throws SQLException {
		LinkedList<Badge> badges = new LinkedList<Badge>();

		ResultSet queryResult = db.cachedQuery(AsiaticoQ.LOAD_BADGES_FOR_CAFE_ID, cafe.getId());
		while (queryResult.next())
			badges.add(new Badge(cafe.getId(), queryResult.getString(DbKeyC.BADGE_TYPE)));

		return badges;
	}

	/**
	 * Opens a ticket.
	 * 
	 * @param ticket to open
	 */
	public OpenTicketResult openTicket(Ticket ticket) throws BaristaException, SQLException {
		String userId = ticket.getUserId();

		TicketResultBuilder trb = new TicketResultBuilder();

		// Execute operation
		switch (ticket.getCompanion()) {
		case ADD_CAFE:
			long pseudoId = IDs.getRandomValidLong(db);
			trb.setTicketId(
					db.cachedGeneratedQuery(AsiaticoQ.TICKET_ADD_CAFE, userId, pseudoId, ticket.getNewCafeName(),
							ticket.getNewLocationLon(), ticket.getNewLocationLat(), ticket.getCompanion().toString()));
			trb.setVirtualCafeId(pseudoId);
			break;

		case REMOVE_CAFE:
			trb.setTicketId(db.cachedGeneratedQuery(AsiaticoQ.TICKET_REMOVE_CAFE, userId, ticket.getAffectedCafeId(),
					ticket.getCompanion().toString()));
			break;

		case CHANGE_CAFE_NAME:
			trb.setTicketId(db.cachedGeneratedQuery(AsiaticoQ.TICKET_CHANGE_CAFE_NAME, userId,
					ticket.getAffectedCafeId(), ticket.getNewCafeName(), ticket.getCompanion().toString()));
			break;

		case CHANGE_CAFE_LOCATION:
			trb.setTicketId(
					db.cachedGeneratedQuery(AsiaticoQ.TICKET_CHANGE_CAFE_LOCATION, userId, ticket.getAffectedCafeId(),
							ticket.getNewLocationLat(), ticket.getNewLocationLon(), ticket.getCompanion().toString()));
			break;

		case ADD_BADGE:
			trb.setTicketId(db.cachedGeneratedQuery(AsiaticoQ.TICKET_ADD_BADGE, userId, ticket.getAffectedCafeId(),
					ticket.getNewBadgeType().toLowerCase(), ticket.getCompanion().toString()));
			break;

		case REMOVE_BADGE:
			trb.setTicketId(db.cachedGeneratedQuery(AsiaticoQ.TICKET_REMOVE_BADGE, userId, ticket.getAffectedCafeId(),
					ticket.getNewBadgeType().toLowerCase(), ticket.getCompanion().toString()));
			break;

		case CHANGE_CAFE_OPENING_RANGES:
			if (ticket.isHasOpeningChange()) {

				// Create ticket
				PreparedStatement statement = db.getConnection().prepareStatement(
						AsiaticoQ.TICKET_CHANGE_CAFE_OPENING_RANGES, PreparedStatement.RETURN_GENERATED_KEYS);
				Object[] queryParameter = { userId, ticket.getAffectedCafeId(), ticket.getCompanion().toString() };
				for (int i = 0; i < queryParameter.length; i++) {
					statement.setObject(i + 1, queryParameter[i]);
				}
				statement.executeUpdate();

				// Retrieve generated id
				ResultSet idResult = statement.getGeneratedKeys();
				idResult.next();
				trb.setTicketId(idResult.getLong(1));

				// Make sure that opening ranges do not exceed maximum
				if (ticket.getNewOpeningRanges().size() < Constants.OPENING_RANGE_TICKET_LIMIT) {
					List<OpeningRange> ranges = ticket.getNewOpeningRanges();

					// Iterate over ranges
					for (OpeningRange r : ranges) {
						try {
							db.cachedQuery(AsiaticoQ.RANGE_CHANGE_CAFE_OPENING_RANGES, trb.getTicketId(), r.getDay(),
									r.getOpeningTime(), r.getClosingTime());
						} catch (SQLException e) {
							throw Exf.newWrappedArgument(e);
						}
					}
				}
			}
			break;

		case CHANGE_PRODUCT_PRICE:
			trb.setTicketId(
					db.cachedGeneratedQuery(AsiaticoQ.TICKET_CHANGE_PRODUCT_PRICE, userId, ticket.getAffectedCafeId(),
							ticket.getAffectedProductName(), ticket.getAffectedProductSize().toString().toLowerCase(),
							ticket.getNewProductPrice(), ticket.getCompanion().toString()));
			break;

		case DELETE_PRODUCT:
			trb.setTicketId(db.cachedGeneratedQuery(AsiaticoQ.TICKET_DELETE_PRODUCT, userId, ticket.getAffectedCafeId(),
					ticket.getAffectedProductName(), ticket.getAffectedProductSize().toString().toLowerCase(),
					ticket.getCompanion().toString()));
			break;

		case ADD_PRODUCT:
			trb.setTicketId(db.cachedGeneratedQuery(AsiaticoQ.TICKET_ADD_PRODUCT, userId, ticket.getAffectedCafeId(),
					ticket.getAffectedProductName(), ticket.getAffectedProductSize().toString().toLowerCase(),
					ticket.getNewProductPrice(), ticket.getCompanion().toString()));
			break;

		default:
			throw new BaristaException(TextC.INVALID_TICKET_OPERATION_ERR, ErrorType.ARGUMENT);
		}

		return trb.build();
	}

	/**
	 * Queries a specific ticket by ID.
	 * 
	 * @param ticketId to query
	 */
	public Ticket getTicketById(long ticketId) throws SQLException {
		ResultSet queryResult = db.cachedQuery(AsiaticoQ.LOAD_TICKET_BY_ID, ticketId);
		while (queryResult.next()) {

			// Product size
			String currentProdSizeString = queryResult.getString(DbKeyC.AFFECTED_PRODUCT_SIZE);
			ProductSize currentProdSize = null;
			if (currentProdSizeString != null)
				currentProdSize = ProductSize.valueOf(currentProdSizeString);

			// Status
			TicketStatus status = null;
			boolean accepted = Boolean.valueOf(queryResult.getBoolean(DbKeyC.ACCEPTED));
			if (queryResult.wasNull())
				status = TicketStatus.UNRESOLVED;
			else if (accepted)
				status = TicketStatus.ACCEPTED;
			else if (!accepted)
				status = TicketStatus.REJECTED;

			// Load opening ranges
			LinkedList<OpeningRange> newOpeningRanges = new LinkedList<>();
			if (queryResult.getBoolean(DbKeyC.HAS_OPENING_CHANGE)) {
				ResultSet newOpeningRangesResult = db.cachedQuery(AsiaticoQ.LOAD_NEW_OPENING_RANGES, ticketId);
				while (newOpeningRangesResult.next())
					newOpeningRanges.add(new OpeningRange(newOpeningRangesResult.getString(DbKeyC.DAY_OF_WEEK),
							newOpeningRangesResult.getTimestamp(DbKeyC.OPENING_TIME),
							newOpeningRangesResult.getTimestamp(DbKeyC.CLOSING_TIME)));
			}

			// Companion type
			String currentCompanionString = queryResult.getString(DbKeyC.COMPANION_TYPE);
			CompanionType currentCompanion = null;
			if (currentCompanionString != null)
				currentCompanion = CompanionType.valueOf(currentCompanionString);

			return new Ticket(queryResult.getString(DbKeyC.USER_ID), queryResult.getLong(DbKeyC.AFFECTED_CAFE_ID),
					queryResult.getString(DbKeyC.AFFECTED_PRODUCT_NAME), currentProdSize,
					queryResult.getString(DbKeyC.NEW_BADGE_TYPE), queryResult.getDouble(DbKeyC.NEW_LOCATION_LON),
					queryResult.getDouble(DbKeyC.NEW_LOCATION_LAT), queryResult.getString(DbKeyC.NEW_CAFE_NAME),
					queryResult.getDouble(DbKeyC.NEW_PRODUCT_PRICE), currentCompanion, status, newOpeningRanges);
		}
		return null;
	}

	/**
	 * Applies a {@link CafeRating}.
	 */
	public void setRating(CafeRating rating) throws SQLException {
		db.cachedQuery(AsiaticoQ.RATING_SET, rating.getCafeId(), rating.getUsername(), rating.getRating());
	}

	/**
	 * Adds a {@link User}.
	 */
	public void addUser(User user) throws SQLException {
		db.cachedQuery(AsiaticoQ.USER_REGISTER, user.getUserId(), user.getIp());
	}
}
