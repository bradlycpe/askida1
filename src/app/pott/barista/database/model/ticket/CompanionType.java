package app.pott.barista.database.model.ticket;

import org.json.JSONObject;

import app.pott.barista.Constants.AsiaticoTicketC;

public enum CompanionType {
	ADD_CAFE(AsiaticoTicketC.ADD_CAFE_SELECT, AsiaticoTicketC.ADD_CAFE_CHANGE),
	REMOVE_CAFE(AsiaticoTicketC.REMOVE_CAFE_SELECT, AsiaticoTicketC.REMOVE_CAFE_CHANGE),
	CHANGE_CAFE_NAME(AsiaticoTicketC.CHANGE_CAFE_NAME_SELECT, AsiaticoTicketC.CHANGE_CAFE_NAME_CHANGE),
	ADD_BADGE(AsiaticoTicketC.ADD_BADGE_SELECT, AsiaticoTicketC.ADD_BADGE_CHANGE),
	REMOVE_BADGE(AsiaticoTicketC.REMOVE_BADGE_SELECT, AsiaticoTicketC.REMOVE_BADGE_CHANGE),
	CHANGE_CAFE_OPENING_RANGES(AsiaticoTicketC.CHANGE_CAFE_OPENING_RANGES_SELECT,
			AsiaticoTicketC.CHANGE_CAFE_OPENING_RANGES_CHANGE),
	CHANGE_PRODUCT_PRICE(AsiaticoTicketC.CHANGE_PRODUCT_PRICE_SELECT, AsiaticoTicketC.CHANGE_PRODUCT_PRICE_CHANGE),
	DELETE_PRODUCT(AsiaticoTicketC.DELETE_PRODUCT_SELECT, AsiaticoTicketC.DELETE_PRODUCT_CHANGE),
	ADD_PRODUCT(AsiaticoTicketC.ADD_PRODUCT_SELECT, AsiaticoTicketC.ADD_PRODUCT_CHANGE),
	CHANGE_CAFE_LOCATION(AsiaticoTicketC.CHANGE_CAFE_LOCATION_SELECT, AsiaticoTicketC.CHANGE_CAFE_LOCATION_CHANGE);

	private final TicketArgumentValidator validator;
	private String[] selectKeys;
	private String[] changeKeys;

	private CompanionType(final String[] selectKeys, final String[] changeKeys) {
		this.selectKeys = selectKeys;
		this.changeKeys = changeKeys;
		validator = new TicketArgumentValidator(selectKeys, changeKeys);
	}

	public boolean isValid(JSONObject args) {
		return validator.isValid(args);
	}

	public String[] getChangeKeys() {
		return changeKeys;
	}

	public String[] getSelectKeys() {
		return selectKeys;
	}

}
