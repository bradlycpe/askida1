package app.pott.barista.database.model.entities;

public class Badge {
	private long cafeId;
	private String badgeType;

	public Badge(long cafeId, String badgeType) {
		this.cafeId = cafeId;
		this.badgeType = badgeType;
	}

	public String getBadgeType() {
		return badgeType;
	}

	public long getCafeId() {
		return cafeId;
	}
}
