package app.pott.barista.database.model.entities;

public class LocalizedProductType {
	private String typeName;
	private String translation;

	public LocalizedProductType(String typeName, String translation) {
		this.typeName = typeName;
		this.translation = translation;
	}

	public String getTypeNameLocalized() {
		return translation;
	}

	public String getTypeName() {
		return typeName;
	}
}
