package app.pott.barista.http;

public enum RequestType {
	POST, GET, PUT;
}
