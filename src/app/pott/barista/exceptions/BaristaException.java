package app.pott.barista.exceptions;

public class BaristaException extends Exception {

	private static final long serialVersionUID = -3633622838202351776L;
	private ErrorType type;

	public BaristaException(String message, ErrorType type) {
		super(message);
		this.type = type;
	}

	public BaristaException(Throwable t, ErrorType type) {
		super(t);
		this.type = type;
	}

	public BaristaException(ErrorType type) {
		super(type.toString());
		this.type = type;
	}

	public ErrorType getType() {
		return type;
	}

	@Override
	public String toString() {
		return type + " -> " + getMessage();
	}
}
