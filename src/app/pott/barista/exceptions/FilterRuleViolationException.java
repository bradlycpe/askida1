package app.pott.barista.exceptions;

public class FilterRuleViolationException extends Exception {

	private static final long serialVersionUID = -7323067911705561004L;

	public FilterRuleViolationException(String message) {
		super(message);
	}
}
