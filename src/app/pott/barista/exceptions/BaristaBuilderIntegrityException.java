package app.pott.barista.exceptions;

public class BaristaBuilderIntegrityException extends BaristaException {

	private static final long serialVersionUID = -7637631752890579802L;

	public BaristaBuilderIntegrityException(Class<?> builderClass) {
		super("Integrity violated in class '" + builderClass.getName() + "'", ErrorType.INTERNAL);
	}

}
